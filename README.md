# useLangDefs

> Simple Java app to highlight example code using GitNex' language definitions for debugging and adding themes and
> languages.

### Dependencies

Requires OpenJDK 17 (or a compatible JDK).

### How this works

This mainly consists of dummy classes replacing the Android classes to make the definitions which target and use Android
work on normal JDKs (of course they're very specific, so you can't really use them in your project).

Output is stored as HTML in `highlighted.html`, highlighting is done via CSS.
Code examples are partially copied from <https://highlightjs.org/static/demo/>.

### Run

1. Clone repo
2. Run `./initialize.sh <path to GitNex root>` (please note that the GitNex root must not end with a slash).
This will create symlinks that will directly edit files for GitNex if you do it for useLangDefs.
3. In `src/main/java/Main`, select the code example and theme you would like to use
4. `./gradlew run`
