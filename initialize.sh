#!/bin/sh

if [ -z "$1" ]; then
    echo "an argument is required"
    exit 1
fi

mkdir src/main/java/org/mian/gitnex/helpers/codeeditor
ln -s "$1"/app/src/main/java/org/mian/gitnex/helpers/codeeditor/languages src/main/java/org/mian/gitnex/helpers/codeeditor/languages
ln -s "$1"/app/src/main/java/org/mian/gitnex/helpers/codeeditor/theme src/main/java/org/mian/gitnex/helpers/codeeditor/theme
