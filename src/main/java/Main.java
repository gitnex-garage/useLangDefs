import android.content.Context;
import com.amrdeveloper.codeview.CodeView;
import org.mian.gitnex.helpers.codeeditor.languages.Language;
import org.mian.gitnex.helpers.codeeditor.theme.Theme;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        CodeExamples code = CodeExamples.C;
        Theme theme = Theme.FIVE_COLORS_DARK;

        CodeView cv = new CodeView();
        Language.fromName(code.lang).applyTheme(new Context(), cv, theme);

        File op = new File("highlighted.html");
        op.createNewFile();
        FileWriter fw = new FileWriter(op);
        fw.write(cv.highlight(code.code));
        fw.close();

        if (Desktop.isDesktopSupported()) {
            Desktop.getDesktop().open(op);
        }
    }
}