package com.amrdeveloper.codeview;

import org.apache.commons.text.StringEscapeUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CodeView {
    public void resetHighlighter() {
        bgColor = 0;
        txColor = 0;
    }

    public void resetSyntaxPatternList() {
        patterns = new LinkedHashMap<>();
    }

    public void reHighlightSyntax() {
    }

    private int bgColor;
    private int txColor;
    private LinkedHashMap<Pattern, Integer> patterns = new LinkedHashMap<>();

    public void setBackgroundColor(int color) {
        bgColor = color;
    }

    public void addSyntaxPattern(Pattern p, int color) {
        patterns.put(p, color);
    }

    public void setTextColor(int color) {
        txColor = color;
    }

    public String highlight(String code) {

        return """
                <!DOCTYPE html>
                <html>
                <body style="background: #%1$s;color: #%2$s">
                <code style="white-space: break-spaces">
                %3$s
                </code>
                </body>
                </html>
                """.formatted(Integer.toHexString(bgColor), Integer.toHexString(txColor), applyPatterns(code));
    }

    private class Char {
        public final char c;
        public int color = txColor;

        private Char(char c) {
            this.c = c;
        }
    }

    private String applyPatterns(String code) {
        ArrayList<Char> colors = new ArrayList<>();

        code.chars().forEach(value -> colors.add(new Char((char) value)));

        patterns.forEach((p, color) -> {
            Matcher matcher = p.matcher(code);
            while(matcher.find()) {
                for (int i = matcher.start(); i < matcher.end(); i++) {
                    colors.get(i).color = color;
                }
            }
        });

        StringBuilder html = new StringBuilder();

        AtomicInteger currentColor = new AtomicInteger(-1);
        colors.forEach(c -> {
            if (c.color == currentColor.get()) {
                html.append(StringEscapeUtils.escapeHtml4(String.valueOf(c.c)));
            } else {
                if (currentColor.get() != -1) html.append("</span>");
                html.append("<span style=\"color: #%s\">".formatted(Integer.toHexString(c.color)));
                html.append(StringEscapeUtils.escapeHtml4(String.valueOf(c.c)));
                currentColor.set(c.color);
            }
        });

        return html.append("</span>").toString();
    }
}
