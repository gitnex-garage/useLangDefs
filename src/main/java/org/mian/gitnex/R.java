package org.mian.gitnex;

public class R {

    public static Attrs attr = new Attrs();
    public static Colors color = new Colors();

    public static class Attrs {

        public int isDark = 1;
    }
    
    public static class Colors {

        public int five_dark_purple = 0xeb84f3;
        public int five_dark_yellow = 0xfde92f;
        public int five_dark_white = 0xffffff;
        public int five_dark_grey = 0xa9b1ae;

        public int five_dark_blue = 0x267ae9;
        public int five_dark_black = 0x252526;
        public int five_yellow = 0xfdc92f;
        public int five_background_grey = 0xf5f2f0;
    }
}
