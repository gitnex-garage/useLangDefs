import com.amrdeveloper.codeview.Code;

public class CodeExamples {

    public final String code;
    public final String lang;

    public static CodeExamples PHP = new CodeExamples("""
            require_once 'Zend/Uri/Http.php';
                        
            namespace Location\\Web;
                        
            interface Factory
            {
                static function _factory();
            }
                        
            abstract class URI extends BaseURI implements Factory
            {
                abstract function test();
                        
                public static $st1 = 1;
                const ME = "Yo";
                var $list = NULL;
                private $var;
                        
                /**
                 * Returns a URI
                 *
                 * @return URI
                 */
                static public function _factory($stats = array(), $uri = 'http')
                {
                    echo __METHOD__;
                    $uri = explode(':', $uri, 0b10);
                    $schemeSpecific = isset($uri[1]) ? $uri[1] : '';
                    $desc = 'Multi
            line description';
                        
                    // Security check
                    if (!ctype_alnum($scheme)) {
                        throw new Zend_Uri_Exception('Illegal scheme');
                    }
                        
                    $this->var = 0 - self::$st;
                    $this->list = list(Array("1"=> 2, 2=>self::ME, 3 => \\Location\\Web\\URI::class));
                        
                    return [
                        'uri'   => $uri,
                        'value' => null,
                    ];
                }
            }
                        
            match ($key) {
                1 => 'Integer 1',
                '1' => 'String 1',
                true => 'Bool true',
                [] => 'Empty array',
                [1] => 'Array [1]',
            };
                        
            enum Foo: string {
                case Test = 'test';
            }
                        
            echo URI::ME . URI::$st1;
                        
            __halt_compiler () ; datahere
            datahere
            datahere */
            datahere""", "php");

    public static CodeExamples PHP_INLINE = new CodeExamples("""
            <html>
            </html>
            <body>
            <?php
            # Here echo command is used to print
            echo "Hello, world!";
            ?>
            </body>""", "php");

    public static CodeExamples JAVA = new CodeExamples("""
            /**
             * @author John Smith <john.smith@example.com>
             */
            package l2f.gameserver.model;
            
            public abstract strictfp class L2Char extends L2Object {
              public static final Short ERROR = 0x0001;
            
              public void moveTo(int x, int y, int z) {
                _ai = null;
                log("Should not be called");
                if (1 > 5) { // wtf!?
                  return;
                }
              }
            }""", "java");

    public static CodeExamples GO = new CodeExamples("""
            package main
             
            import "fmt"
            
            func main() {
                ch := make(chan float64)
                ch <- 1.0e10    // magic number
                x, ok := <- ch
                defer fmt.Println(`exitting\\` now\\`)
                go println(len("hello world!"))
                return
            }""", "go");

    public static CodeExamples HTML = new CodeExamples("""
            <!DOCTYPE html>
            <title>Title</title>

            <style>body {width: 500px;}</style>

            <script type="application/javascript">
              function $init() {return true;}
            </script>

            <body>
              <p checked class="title" id='title'>Title</p>
              <!-- here goes the rest of the page -->
            </body>""", "html");

    public static CodeExamples XML = new CodeExamples("""
            <!DOCTYPE html>
            <title>Title</title>

            <style>body {width: 500px;}</style>

            <script type="application/javascript">
              function $init() {return true;}
            </script>

            <body>
              <p checked class="title" id='title'>Title</p>
              <!-- here goes the rest of the page -->
            </body>""", "xml");

    public static CodeExamples PYTHON = new CodeExamples("""
            @requires_authorization(roles=["ADMIN"])
            def somefunc(param1='', param2=0):
                r'''A docstring'''
                if param1 > param2: # interesting
                    print 'Gre\\'ater'
                return (param2 - param1 + 1 + 0b10l) or None

            class SomeClass:
                pass

            >>> message = '''interpreter
            ... prompt'''""", "python");

    public static CodeExamples JAVASCRIPT = new CodeExamples("""
            function $initHighlight(block, cls) {
              try {
                if (cls.search(/\\bno\\-highlight\\b/) != -1)
                  return process(block, true, 0x0F) +
                         ` class="${cls}"`;
              } catch (e) {
                /* handle exception */
              }
              for (var i = 0 / 2; i < classes.length; i++) {
                if (checkCondition(classes[i]) === undefined)
                  console.log('undefined');
              }
                        
              return (
                <div>
                  <web-component>{block}</web-component>
                </div>
              )
            }
                        
            export  $initHighlight;""", "javascript");

    public static CodeExamples TYPESCRIPT = new CodeExamples("""
            class MyClass {
              public static myValue: string;
              constructor(init: string) {
                this.myValue = init;
              }
            }
            import fs = require("fs");
            module MyModule {
              export interface MyInterface extends Other {
                myProperty: any;
              }
            }
            declare magicNumber number;
            myArray.forEach(() => { }); // fat arrow syntax""", "typescript");

    public static CodeExamples JSON = new CodeExamples("""
            [
              {
                "title": "apples",
                "count": [12000, 20000],
                "description": {"text": "...", "sensitive": false}
              },
              {
                "title": "oranges",
                "count": [17500, null],
                "description": {"text": "...", "sensitive": false}
              }
            ]""", "json");

    public static CodeExamples CPP = new CodeExamples("""
            #include <iostream>
                        
            int main(int argc, char *argv[]) {
                        
              /* An annoying "Hello World" example */
              for (auto i = 0; i < 0xFFFF; i++)
                cout << "Hello, World!" << endl;
                        
              char c = '\\n';
              unordered_map <string, vector<string> > m;
              m["key"] = "\\\\\\\\"; // this is an error
                        
              return -2e3 + 12l;
            }""", "cpp");

    public static CodeExamples C = new CodeExamples("""
            #include <stdio.h>
            int main() {
               int i, space, rows, k = 0;
               printf("Enter the number of rows: ");
               scanf("%d", &rows);
               for (i = 1; i <= rows; ++i, k = 0) {
                  for (space = 1; space <= rows - i; ++space) {
                     printf("  ");
                  }
                  while (k != 2 * i - 1) {
                     printf("* ");
                     ++k;
                  }
                  printf("\\n");
               }
               return 0;
            }""", "c");

    public CodeExamples(String code, String lang) {
        this.code = code;
        this.lang = lang;
    }
}
